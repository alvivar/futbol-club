﻿using System.Collections;
using UnityEngine;
using TouchScript;

public class CameraDrag : MonoBehaviour
{
    [Header("Settings")]
    public float dragDivisionSpeed = 20;
    public Vector2[] clampCameraPosition;
    public int scenarioIndex = -1;

    [Header("Required")]
    public UiMainMenu uiManager;

    private Vector3 camPosition;

    void OnEnable ()
    {
        if(TouchManager.Instance != null)
        {
            TouchManager.Instance.TouchesBegan += StartTouch;
            TouchManager.Instance.TouchesMoved += DragCamera;
        }
    }

    void OnDisable()
    {
        if (TouchManager.Instance != null)
        {
            TouchManager.Instance.TouchesBegan -= StartTouch;
            TouchManager.Instance.TouchesMoved -= DragCamera;
        }
    }

    void StartTouch(object sender, TouchEventArgs e)
    {
        // Make this script independant
        if(uiManager == null)
        {
            return;
        }

        // Tell if we are on a scenario where we can drag the camera

        // Are we on the inventory scene?
        if(uiManager.lastMainButton == uiManager.inventoryButton)
        {
            scenarioIndex = 0;
        }
        // Are we on the store?
        else if (uiManager.lastMainButton == uiManager.storeButton)
        {
            scenarioIndex = 1;
        }
        else
        {
            scenarioIndex = -1;
        }
    }

    void DragCamera (object sender, TouchEventArgs e)
    {
        // Should we drag the camera?
        if(scenarioIndex < 0)
        {
            return;
        }

        // Drag camera with clamped movement
        for(int i = 0; i < e.Touches.Count; i++)
        {
            camPosition = transform.localPosition;
            camPosition.y -= (e.Touches[i].Position.y - e.Touches[i].PreviousPosition.y) / dragDivisionSpeed;
            camPosition.y = Mathf.Clamp(camPosition.y, clampCameraPosition[scenarioIndex].x, clampCameraPosition[scenarioIndex].y);
            transform.localPosition = camPosition;
            Debug.Log((e.Touches[i].Position.y - e.Touches[i].PreviousPosition.y) / dragDivisionSpeed);
        }
    }
}
