﻿
// Main menu!


using UnityEngine;
using UnityEngine.UI;
using DG.Tweening;

public class UiMainMenu : MonoBehaviour
{
    [Header("Transition Cams")]
    public Camera mainCam;
    public Camera homeCam;
	public Camera trainingCam;// jose vargas was here
	public Camera playCam;//JV and here
    public Camera inventoryCam;
    public Camera storeCam;

    [Header("Required")]
    public Sprite passiveSprite; // Both are used to change the button background during interactions
    public Sprite activeSprite;

    public Animator trainingPanel;

    [Header("Children")]
    public Button homeButton;
    public Button trainingButton;
    public Button playGameButton;
    public Button inventoryButton;
    public Button storeButton;

    public Button lastMainButton;


    void Start()
    {
        // Set the main button as the main game button, wats
        SetMainButton(homeButton, playGameButton);
        lastMainButton = homeButton;

        // ==== Events ======

        homeButton.onClick.AddListener(() =>
        {
            if (lastMainButton == homeButton) return;

            SetMainCamera(mainCam, homeCam, lastMainButton);

            SetMainButton(homeButton, lastMainButton);
            lastMainButton = homeButton;
        });

        trainingButton.onClick.AddListener(() =>
        {
            if (lastMainButton == trainingButton) return;

				//jose vargas was here
            SetMainCamera(mainCam, trainingCam, lastMainButton);
			
            SetMainButton(trainingButton, lastMainButton);
            lastMainButton = trainingButton;
        });

        playGameButton.onClick.AddListener(() =>
        {
            if (lastMainButton == playGameButton) return;

				//jose vargas was here
            SetMainCamera(mainCam, playCam, lastMainButton);

            SetMainButton(playGameButton, lastMainButton);
            lastMainButton = playGameButton;
        });

        inventoryButton.onClick.AddListener(() =>
        {
            if (lastMainButton == inventoryButton) return;

            SetMainCamera(mainCam, inventoryCam, lastMainButton);

            SetMainButton(inventoryButton, lastMainButton);
            lastMainButton = inventoryButton;
        });

        storeButton.onClick.AddListener(() =>
        {
            if (lastMainButton == storeButton) return;

            SetMainCamera(mainCam, storeCam, lastMainButton);

            SetMainButton(storeButton, lastMainButton);
            lastMainButton = storeButton;
        });
    }


    public void SetMainCamera(Camera main, Camera to, Button lastBtn)
    {
        // Ignore when they are the same
        if (main.transform.position == to.transform.position)
            return;

        // Relative direction from both cameras
        var xDir = (main.transform.position.x - to.transform.position.x);
        xDir = xDir < 0 ? -1 : 1;

        DOTween.Sequence()
            // Punch
            .Insert(0f, main.transform.DOMove(to.transform.position + new Vector3(xDir * -1 * 0.1f, 0, 0), 0.6f))
            .Insert(0f, main.transform.DORotate(to.transform.eulerAngles + new Vector3(0, 0, xDir), 0.6f))
            // Change field of view with respective camera
            .Insert(0f, main.DOFieldOfView(to.fieldOfView, 0.6f))

            // First check if we have to hide training panel
            .InsertCallback(0.01f, () => HideTrainingPanel(to, (int)xDir, lastBtn))
            .InsertCallback(0.02f, () => { trainingPanel.SetInteger("Direction", 0); })

            // If we are going to gym
            .InsertCallback(0.1f, () => ShowTrainingPanel(to, (int)xDir))
            .InsertCallback(0.2f, () => { trainingPanel.SetInteger("Direction", 0); })
            // Fit
            .Insert(0.6f, main.transform.DOMove(to.transform.position, 0.2f))
            .Insert(0.6f, main.transform.DORotate(to.transform.eulerAngles, 0.2f));
                 
    }

    public void HideTrainingPanel (Camera to, int dir, Button lastBtn)
    {
        // Only if we were on te gym
        if (to != trainingCam && lastBtn == trainingButton)
        {
            trainingPanel.SetInteger("Direction", dir);
        }
    }

    public void ShowTrainingPanel(Camera to, int dir)
    {
        // Show training panel on gym section only
        if (to == trainingCam)
        {
            trainingPanel.SetInteger("Direction", dir);
        }
    }


    public void SetMainButton(Button main, Button unset)
    {
        // Main button stuff
        var mainLayout = main.GetComponent<LayoutElement>();
        var mainImage = main.transform.Find("Image").GetComponentInChildren<Image>();
        var mainImageRect = mainImage.GetComponent<RectTransform>();
        var mainText = main.transform.GetComponentInChildren<Text>();

        // Main grows
        var mainUp = DOTween.Sequence()
            .Insert(0, mainText.DOColor(Color.white, 0.20f))

            .Insert(0, DOTween.To(() => mainLayout.flexibleWidth, x => mainLayout.flexibleWidth = x, 1.75f, 0.10f))
            .Insert(0.10f, DOTween.To(() => mainLayout.flexibleWidth, x => mainLayout.flexibleWidth = x, 1.5f, 0.20f))

            .Insert(0, DOTween.To(() => mainImageRect.sizeDelta, x => mainImageRect.sizeDelta = x, new Vector2(130, 130), 0.10f))
            .Insert(0, mainImageRect.DOLocalMove(new Vector3(0, 40, 0), 0.10f))

            .Insert(0.10f, DOTween.To(() => mainImageRect.sizeDelta, x => mainImageRect.sizeDelta = x, new Vector2(100, 100), 0.20f))
            .Insert(0.10f, mainImageRect.DOLocalMove(new Vector3(0, 50, 0), 0.20f));

        // Main button background
        main.GetComponent<Image>().sprite = activeSprite;


        // Unset will return to normal, probably is the last used
        if (unset != null)
        {
            var unsetLayout = unset.GetComponent<LayoutElement>();
            var unsetImage = unset.transform.Find("Image").GetComponentInChildren<Image>();
            var unsetImageRect = unsetImage.GetComponent<RectTransform>();
            var unsetText = unset.transform.GetComponentInChildren<Text>();

            var unsetDown = DOTween.Sequence()
                .Insert(0, DOTween.To(() => unsetLayout.flexibleWidth, x => unsetLayout.flexibleWidth = x, 1f, 0.20f))
                .Insert(0, unsetImageRect.DOLocalMove(new Vector3(0, 0, 0), 0.20f))
                .Insert(0, DOTween.To(() => unsetImageRect.sizeDelta, x => unsetImageRect.sizeDelta = x, new Vector2(70, 70), 0.20f))
                .Insert(0, unsetText.DOColor(Color.clear, 0.20f));

            // Button background
            unset.GetComponent<Image>().sprite = passiveSprite;
        }
    }
}
